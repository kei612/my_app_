//
//  animation2.m
//  秘密のメモ〜電卓編
//
//  Created by 伊藤慶 on 2016/07/15.
//  Copyright © 2016年 伊藤慶. All rights reserved.
//

#import "animation2.h"

@implementation animation2
//下
- (void)animateTransition:(id<UIViewControllerContextTransitioning>)transitionContext {
    UIViewController *fromVC = [transitionContext viewControllerForKey:UITransitionContextFromViewControllerKey];
    UIViewController *toVC = [transitionContext viewControllerForKey:UITransitionContextToViewControllerKey];
    CGRect fromRect = [transitionContext initialFrameForViewController:fromVC];
    
    UIView* inView = [transitionContext containerView];
    NSInteger direction = ((self.action == KWTransitionStepPresent && !(self.settings & KWTransitionSettingReverse)) ||
                           (self.action == KWTransitionStepDismiss && (self.settings & KWTransitionSettingReverse))) ? 1 : -1;
    
    fromVC.view.frame = fromRect;
    [inView addSubview:toVC.view];
    
    CGPoint toCenter = inView.center;
    toCenter.y += direction * CGRectGetHeight(inView.bounds);
    toVC.view.center = toCenter;
    
    [UIView animateWithDuration:2 delay:.0f usingSpringWithDamping:2.8f initialSpringVelocity:2 options:UIViewAnimationOptionCurveEaseIn animations:^{
        CGPoint fromCenter = inView.center;
        fromCenter.y -= direction * CGRectGetHeight(inView.bounds);
        fromVC.view.center = fromCenter;
        toVC.view.center = inView.center;
    } completion:^(__unused BOOL finished) {
        [transitionContext completeTransition:YES];
    }];
}

- (NSTimeInterval)transitionDuration:(id<UIViewControllerContextTransitioning>)transitionContext {
    return 2.0;
}

@end
