//
//  tutorial7.h
//  秘密のメモ〜電卓編
//
//  Created by 伊藤慶 on 2016/07/16.
//  Copyright © 2016年 伊藤慶. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface tutorial7 : UIViewController
{
    NSMutableArray *array;
    UIImage *image[8];
    UIImageView *views[4];
}
@property (weak, nonatomic) IBOutlet UIImageView *imageview1;
@property (weak, nonatomic) IBOutlet UIImageView *imageview2;
@property (weak, nonatomic) IBOutlet UIImageView *imageview3;
@property (weak, nonatomic) IBOutlet UIImageView *imageview4;

@end
