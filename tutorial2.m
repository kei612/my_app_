//
//  tutorial2.m
//  秘密のメモ〜電卓編
//
//  Created by 伊藤慶 on 2016/07/15.
//  Copyright © 2016年 伊藤慶. All rights reserved.
//

#import "tutorial2.h"

@interface tutorial2 ()

@end

@implementation tutorial2

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.transitioningDelegate=self;
    _isFadeIn = YES;
    NSTimer *tm =
    [NSTimer
     scheduledTimerWithTimeInterval:1.5
     target:self
     selector:@selector(FadeInOut:)
     userInfo:nil
     repeats:YES
     ];
    //下向きのSwipe（1本指でなぞる）の認識
    UISwipeGestureRecognizer* swipeDownRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeDownDetected:)];
    swipeDownRecognizer.direction = UISwipeGestureRecognizerDirectionDown;
    [self.view addGestureRecognizer:swipeDownRecognizer];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
//下向きスワイプ検知時に呼ばれるメソッド
- (IBAction)swipeDownDetected:(UIGestureRecognizer *)sender {
    NSLog(@"下向きSwipe");
 [self performSegueWithIdentifier:@"step2" sender:self];
    
}
-(void)FadeInOut:(NSTimer*)timer{
    if (_isFadeIn) {
        //フェードアウトアニメーション実行
        [self sampleImageFadeOut];
    } else {
        //フェードインアニメーション実行
        [self sampleImageFadeIn];
    }
    _isFadeIn = !_isFadeIn;
    
}

- (void)sampleImageFadeIn
{
    //フェードイン
    _ImageView.alpha = 0;
    //アニメーションのタイプを指定
    [UIView beginAnimations:@"fadeIn" context:nil];
    //イージング指定
    [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
    //アニメーション秒数を指定
    [UIView setAnimationDuration:0.9];
    //目標のアルファ値を指定
    _ImageView.alpha = 1;
    //アニメーション実行
    [UIView commitAnimations];
}

- (void)sampleImageFadeOut
{
    //フェードアウト
    [UIView beginAnimations:@"fadeOut" context:nil];
    //イージング指定
    [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
    //アニメーション秒数を指定
    [UIView setAnimationDuration:0.8];
    //目標のアルファ値を指定
    _ImageView.alpha = 0;
    //アニメーション実行
    [UIView commitAnimations];
}
- (id <UIViewControllerAnimatedTransitioning>)animationControllerForPresentedController:(UIViewController *)presented
                                                                   presentingController:(UIViewController *)presenting
                                                                       sourceController:(UIViewController *)source
{
    animation3 *animation=[[animation3 alloc]init];
    return animation;
    
}

- (id <UIViewControllerAnimatedTransitioning>)animationControllerForDismissedController:(UIViewController *)dismissed
{
     animation3 *animation=[[animation3 alloc]init];
    return animation;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
