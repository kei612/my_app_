//
//  tutorial3.m
//  秘密のメモ〜電卓編
//
//  Created by 伊藤慶 on 2016/07/15.
//  Copyright © 2016年 伊藤慶. All rights reserved.
//

#import "tutorial3.h"

@interface tutorial3 ()

@end

@implementation tutorial3

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.transitioningDelegate=self;
    _isFadeIn = YES;
    NSTimer *tm =
    [NSTimer
     scheduledTimerWithTimeInterval:1.5
     target:self
     selector:@selector(FadeInOut:)
     userInfo:nil
     repeats:YES
     ];
    //右向きのSwipe（1本指でなぞる）の認識
    UISwipeGestureRecognizer *swipeRightRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeRightDetected:)];
    swipeRightRecognizer.direction = UISwipeGestureRecognizerDirectionRight;
    [self.view addGestureRecognizer:swipeRightRecognizer];
}
- (void)viewDidAppear:(BOOL)animated
{
    NSLog(@"viewDidAppear");
    [super viewDidAppear:animated];

    [UIView animateWithDuration:2.0 // 4秒かけてアニメーション
                     animations:^
     {
         CGRect frame = _imageview.frame;
         frame.origin.x = 200; // 右に100移動
         _imageview.frame = frame;
//         _imageview2.alpha = 0.0;
     }];}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//左向きスワイプ検知時に呼ばれるメソッド
- (IBAction)swipeRightDetected:(UIGestureRecognizer *)sender {
    NSLog(@"右向きSwipe");
    [self performSegueWithIdentifier:@"step3" sender:self];
}
-(void)FadeInOut:(NSTimer*)timer{
    if (_isFadeIn) {
        //フェードアウトアニメーション実行
        [self sampleImageFadeOut];
    } else {
        //フェードインアニメーション実行
        [self sampleImageFadeIn];
    }
    _isFadeIn = !_isFadeIn;
    
}

- (void)sampleImageFadeIn
{
    //フェードイン
    _ImageView.alpha = 0;
    //アニメーションのタイプを指定
    [UIView beginAnimations:@"fadeIn" context:nil];
    //イージング指定
    [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
    //アニメーション秒数を指定
    [UIView setAnimationDuration:0.9];
    //目標のアルファ値を指定
    _ImageView.alpha = 1;
    //アニメーション実行
    [UIView commitAnimations];
}

- (void)sampleImageFadeOut
{
    //フェードアウト
    [UIView beginAnimations:@"fadeOut" context:nil];
    //イージング指定
    [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
    //アニメーション秒数を指定
    [UIView setAnimationDuration:0.8];
    //目標のアルファ値を指定
    _ImageView.alpha = 0;
    //アニメーション実行
    [UIView commitAnimations];
}
- (id <UIViewControllerAnimatedTransitioning>)animationControllerForPresentedController:(UIViewController *)presented
                                                                   presentingController:(UIViewController *)presenting
                                                                       sourceController:(UIViewController *)source
{
    animation5 *animation=[[animation5 alloc]init];
    return animation;
    
}

- (id <UIViewControllerAnimatedTransitioning>)animationControllerForDismissedController:(UIViewController *)dismissed
{
    animation5 *animation=[[animation5 alloc]init];
    return animation;
}

@end
