//
//  SettingTableViewController.swift
//  noteサンプル
//
//  Created by 伊藤慶 on 2016/09/25.
//  Copyright © 2016年 伊藤慶. All rights reserved.
//

import UIKit

class SettingTableViewController: UITableViewController {

    @IBOutlet weak var fontname: UILabel!
    @IBOutlet weak var fontlabel: UILabel!
    @IBOutlet weak var fontsize: UISlider!
    @IBOutlet weak var fontsample: UITextView!
    var UserDafault:UserDefaults = UserDefaults()
    override func viewDidLoad() {
        super.viewDidLoad()

      fontsize.minimumValue = 18
      fontsize.maximumValue = 44
        }
    override func viewWillAppear(_ animated: Bool) {
        if UserDafault.integer(forKey: "size") < 18 {
            fontsize.value = 18
            fontsample.font = UIFont.systemFont(ofSize: CGFloat(18))
            fontlabel.text = String("フォントサイズ:20")
        }else{
            fontsize.value = Float(UserDafault.integer(forKey: "size"))
            fontlabel.text = String("フォントサイズ:\(UserDafault.integer(forKey: "size"))")
            fontsample.font = UIFont(name:UserDafault.object(forKey: "font") as! String,size:CGFloat(UserDafault.integer(forKey: "size")))
            fontname.text = UserDafault.object(forKey: "fontname") as! String?
            
        }
    }
    @IBAction func size(_ sender: UISlider) {
        if UserDafault.object(forKey: "font") == nil {
            fontsample.font = UIFont.systemFont(ofSize:
                CGFloat(sender.value))
            UserDafault.set(fontsample.font?.fontName, forKey: "font")
        }else{
            fontsample.font = UIFont(name:UserDafault.object(forKey: "font") as! String,size:CGFloat(sender.value))
        }
        fontlabel.text = String("フォントサイズ:\(Int(sender.value))")
        UserDafault.set(Int(sender.value), forKey: "size")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func xxxx(segue:UIStoryboardSegue){
        
    }
    
}
