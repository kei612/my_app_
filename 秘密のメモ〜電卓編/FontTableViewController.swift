//
//  FontTableViewController.swift
//  noteサンプル
//
//  Created by 伊藤慶 on 2016/09/25.
//  Copyright © 2016年 伊藤慶. All rights reserved.
//

import UIKit

class FontTableViewController: UITableViewController {
    
    //    @IBOutlet weak var fontlabel: UILabel!
    var UserDafault:UserDefaults = UserDefaults()
    
    let myValues: NSArray = [" Ultra Light","Thin","Light","Regular","Bold","Heavy","Black","Gurmukhi MN","ヒラギノ明朝 ProN","ヒラギノ角ゴシック"]
    let myfont:NSArray = [".SFUIDisplay-Ultralight",
                          ".SFUIDisplay-Thin",
                          ".SFUIDisplay-Light",
                          " .SFUIDisplay",
                          ".SFUIDisplay-Bold",
                          ".SFUIDisplay-Heavy",
                          " .SFUIDisplay-Black",
                          "GurmukhiMN",
                          "HiraMinProN-W3",
                          "HiraginoSans-W3"]
    override func viewDidLoad() {
        super.viewDidLoad()
        //      print(fontlabel.font.fontName)
        
        
        self.tableView.allowsMultipleSelection = false
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Table view data source
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return myValues.count
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        // 再利用するCellを取得する.
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath as IndexPath)
        
        // Cellに値を設定する.
        cell.textLabel!.text = "\(myValues[indexPath.row])"
        cell.textLabel!.font = UIFont(name:myfont[indexPath.row] as! String,size:30)
        
        return cell
    }
    //    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    //        // #warning Incomplete implementation, return the number of rows
    ////        return tableView.count
    //    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath as IndexPath)
        cell?.accessoryType = UITableViewCellAccessoryType.checkmark
        print(cell?.textLabel?.font.fontName)
        UserDafault.set(cell?.textLabel?.font.fontName, forKey: "font")
        UserDafault.set(cell?.textLabel?.text, forKey: "fontname")
        navigationController?.popViewController(animated: true)

        
    }
    override func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath as IndexPath)
        cell?.accessoryType = UITableViewCellAccessoryType.none
        
    }
    
    
    /*
     override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
     let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath)
     
     // Configure the cell...
     
     return cell
     }
     */
    
    /*
     // Override to support conditional editing of the table view.
     override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
     // Return false if you do not want the specified item to be editable.
     return true
     }
     */
    
    /*
     // Override to support editing the table view.
     override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
     if editingStyle == .delete {
     // Delete the row from the data source
     tableView.deleteRows(at: [indexPath], with: .fade)
     } else if editingStyle == .insert {
     // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
     }
     }
     */
    
    /*
     // Override to support rearranging the table view.
     override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {
     
     }
     */
    
    /*
     // Override to support conditional rearranging of the table view.
     override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
     // Return false if you do not want the item to be re-orderable.
     return true
     }
     */
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
