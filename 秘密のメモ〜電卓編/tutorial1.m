//
//  tutorial1.m
//  秘密のメモ〜電卓編
//
//  Created by 伊藤慶 on 2016/07/15.
//  Copyright © 2016年 伊藤慶. All rights reserved.
//

#import "tutorial1.h"

@interface tutorial1 ()

@end

@implementation tutorial1

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    _isFadeIn = YES;
    NSTimer *tm =
    [NSTimer
     scheduledTimerWithTimeInterval:1.5
     target:self
     selector:@selector(FadeInOut:)
     userInfo:nil
     repeats:YES
     ];
    //左向きのSwipe（1本指でなぞる）の認識
    UISwipeGestureRecognizer* swipeLeftRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeLeftDetected:)];
    swipeLeftRecognizer.direction = UISwipeGestureRecognizerDirectionLeft;
    [self.view addGestureRecognizer:swipeLeftRecognizer];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
//左向きスワイプ検知時に呼ばれるメソッド
- (IBAction)swipeLeftDetected:(UIGestureRecognizer *)sender {
    [self performSegueWithIdentifier:@"step1" sender:self];
}
-(void)FadeInOut:(NSTimer*)timer{
    if (_isFadeIn) {
        //フェードアウトアニメーション実行
        [self sampleImageFadeOut];
    } else {
        //フェードインアニメーション実行
        [self sampleImageFadeIn];
    }
    _isFadeIn = !_isFadeIn;

}

- (void)sampleImageFadeIn
{
    //フェードイン
    _ImageView.alpha = 0;
    //アニメーションのタイプを指定
    [UIView beginAnimations:@"fadeIn" context:nil];
    //イージング指定
    [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
    //アニメーション秒数を指定
    [UIView setAnimationDuration:0.9];
    //目標のアルファ値を指定
    _ImageView.alpha = 1;
    //アニメーション実行
    [UIView commitAnimations];
}

- (void)sampleImageFadeOut
{
    //フェードアウト
    [UIView beginAnimations:@"fadeOut" context:nil];
    //イージング指定
    [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
    //アニメーション秒数を指定
    [UIView setAnimationDuration:0.8];
    //目標のアルファ値を指定
    _ImageView.alpha = 0;
    //アニメーション実行
    [UIView commitAnimations];
}
@end
