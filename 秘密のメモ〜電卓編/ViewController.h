//
//  ViewController.h
//  秘密のメモ〜電卓編
//
//  Created by 伊藤慶 on 2016/06/18.
//  Copyright © 2016年 伊藤慶. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController
{
    NSMutableArray *array;
    NSMutableArray *array2;
    BOOL hantei;
}
@property (weak, nonatomic) IBOutlet UITextField *displayCalculation;
@property (nonatomic) double firstNumber;
@property (nonatomic) double secondNumber;
@property (nonatomic) NSString *symbol;
@property (nonatomic) Boolean numStarted;
@property (nonatomic) Boolean numPressed;

- (IBAction)percent;
- (IBAction)clear;
- (IBAction)squareroot;
- (IBAction)decimal:(UIButton *)sender;
- (IBAction)calculation:(UIButton *)sender;
- (IBAction)equals;
- (IBAction)posneg;
- (IBAction)number:(UIButton *)sender;
@end

