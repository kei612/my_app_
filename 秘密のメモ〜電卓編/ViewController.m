//
//  ViewController.m
//  秘密のメモ〜電卓編
//
//  Created by 伊藤慶 on 2016/06/18.
//  Copyright © 2016年 伊藤慶. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    array= [NSMutableArray array];
    array2= [NSMutableArray array];
    

    hantei =YES;
    [self gesture];
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    NSData *data = [ud objectForKey:@"password"];
    array = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"HasLaunched"];
    [[NSUserDefaults standardUserDefaults] synchronize]; //

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)gesture{
    //Pinch（2本の指でつまむ）の認識
    UIPinchGestureRecognizer *pinchRecognizer = [[UIPinchGestureRecognizer alloc] initWithTarget:self action:@selector(pinchDetected:)];
    [self.view addGestureRecognizer:pinchRecognizer];
    
    //Rotate（2本の指で回転）の認識
    UIRotationGestureRecognizer *rotationRecognizer = [[UIRotationGestureRecognizer alloc] initWithTarget:self action:@selector(rotationDetected:)];
    [self.view addGestureRecognizer:rotationRecognizer];
    
    //右向きのSwipe（1本指でなぞる）の認識
    UISwipeGestureRecognizer *swipeRightRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeRightDetected:)];
    swipeRightRecognizer.direction = UISwipeGestureRecognizerDirectionRight;
    [self.view addGestureRecognizer:swipeRightRecognizer];
    
    //左向きのSwipe（1本指でなぞる）の認識
    UISwipeGestureRecognizer* swipeLeftRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeLeftDetected:)];
    swipeLeftRecognizer.direction = UISwipeGestureRecognizerDirectionLeft;
    [self.view addGestureRecognizer:swipeLeftRecognizer];
    
    //上向きのSwipe（1本指でなぞる）の認識
    UISwipeGestureRecognizer *swipeUpRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeUpDetected:)];
    swipeUpRecognizer.direction = UISwipeGestureRecognizerDirectionUp;
    [self.view addGestureRecognizer:swipeUpRecognizer];
    
    //下向きのSwipe（1本指でなぞる）の認識
    UISwipeGestureRecognizer* swipeDownRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeDownDetected:)];
    swipeDownRecognizer.direction = UISwipeGestureRecognizerDirectionDown;
    [self.view addGestureRecognizer:swipeDownRecognizer];
}
-(void)check:(int)count{
    if(hantei==YES){
    if (array2.count<4) {
        [array2 addObject:[NSNumber numberWithInteger:count]];
        if (array2.count==4) {
            for (int i=0;i<array.count; i++) {
                if ([[array objectAtIndex:i] intValue]==[[array2 objectAtIndex:i] intValue]) {
                    
                }else{
                    [array2 removeAllObjects];
                    hantei=NO;
                    break;
                }
            }
            if (hantei==YES) {
                [self performSegueWithIdentifier:@"go" sender:self];
            }
            
        }
    }}
}

- (IBAction)swipeRightDetected:(UIGestureRecognizer *)sender {
    NSLog(@"右向きSwipe");
    [self check:0];
}

//左向きスワイプ検知時に呼ばれるメソッド
- (IBAction)swipeLeftDetected:(UIGestureRecognizer *)sender {
    NSLog(@"左向きSwipe");
    [self check:1];
    
}

//上向きスワイプ検知時に呼ばれるメソッド
- (IBAction)swipeUpDetected:(UIGestureRecognizer *)sender {
    NSLog(@"上向きSwipe");
    [self check:2];
    
}

//下向きスワイプ検知時に呼ばれるメソッド
- (IBAction)swipeDownDetected:(UIGestureRecognizer *)sender {
    NSLog(@"下向きSwipe");
    [self check:3];
    
}

- (IBAction)rotationDetected:(UIGestureRecognizer *)sender {
    //Rotate開始時から見た回転の度合い（ラジアン）
    CGFloat radians = [(UIRotationGestureRecognizer *)sender rotation];
    //「ラジアン」を「度」に変換
    CGFloat degrees = radians * (180/M_PI);
    if (sender.state == UIGestureRecognizerStateEnded) {
        
        
        if (degrees > 90) {
            [self check:4];
            NSLog(@"時計回りにRotate degrees: %f", degrees);
        } else if (degrees < -90) {
            [self check:5];
            NSLog(@"反時計回りにRotate degrees: %f", degrees);
        }
    }
}
//ピンチ動作検知時に呼ばれるメソッド
- (IBAction)pinchDetected:(UIGestureRecognizer *)sender {
    //ピンチ開始の2本の指の距離を1とした時
    //現在の2本の指の相対距離
    CGFloat scale = [(UIPinchGestureRecognizer *)sender scale];
    
    if (scale > 2.4) {
        NSLog(@"外向きにPinch scale: %f", scale);
        if (sender.state == UIGestureRecognizerStateEnded) {
            
            [self check:6];
        }
    } else if (scale < 0.4) {
        NSLog(@"内向きにPinch scale: %f", scale);
        if (sender.state == UIGestureRecognizerStateEnded) {
            
            [self check:7];}
    }
}


- (IBAction)clear {
    [array2 removeAllObjects];
    hantei=YES;
    self.displayCalculation.text = @"0";
    self.firstNumber = 0;
    self.secondNumber = 0;
    self.symbol = @"";
    self.numStarted = NO;
    self.numPressed = NO;
}


- (IBAction)squareroot{
    self.numStarted = NO;
    double num = pow([self.displayCalculation.text doubleValue], .5);
    self.displayCalculation.text = [NSString stringWithFormat:@"%g", num];
}

- (IBAction)number:(UIButton *)sender {
    
    if(self.numStarted){
        if([[self.displayCalculation.text substringToIndex:1] isEqualToString:@"0"] && [self.displayCalculation.text containsString:@"."] == NO){
            self.displayCalculation.text = @"";
            self.displayCalculation.text = sender.currentTitle;
        }
        else{
            self.displayCalculation.text = [self.displayCalculation.text stringByAppendingString:sender.currentTitle];
        }
    }
    else{
        if([sender.currentTitle isEqualToString:@"0"]){
            self.displayCalculation.text = @"0";
        }
        else{
            self.displayCalculation.text = sender.currentTitle;
        }
        self.numStarted = YES;
    }
    
    self.numPressed = YES;
}

- (IBAction)decimal:(UIButton *)sender {
    if(self.numStarted && [self.displayCalculation.text containsString:@"."] == NO){
        self.displayCalculation.text = [self.displayCalculation.text stringByAppendingString:sender.currentTitle];
    }
    else if(self.numStarted == NO){
        self.displayCalculation.text = @"0.";
        self.numStarted = YES;
    }
    
}

- (IBAction)posneg {
    
    if([[self.displayCalculation.text substringToIndex:1] isEqualToString: @"-"]){
        self.displayCalculation.text = [self.displayCalculation.text stringByReplacingOccurrencesOfString:@"-" withString:@""];
    }
    else{
        self.displayCalculation.text = [@"-" stringByAppendingString: self.displayCalculation.text];
    }
}

- (IBAction)percent {
    self.numStarted = NO;
    double percentage = [self.displayCalculation.text doubleValue] * .01;
    self.displayCalculation.text = [NSString stringWithFormat:@"%g", percentage];
}

- (IBAction)calculation:(UIButton *)sender {
    self.numStarted = NO;
    self.firstNumber = [self.displayCalculation.text doubleValue];
    self.symbol = [sender currentTitle];
}

- (IBAction)equals {
    self.numStarted = NO;
    double result = 0;
    
    if(self.numPressed){
        self.secondNumber = [self.displayCalculation.text doubleValue];
        self.numPressed = NO;
    }
    else{
        self.firstNumber = [self.displayCalculation.text doubleValue];
    }
    
    if([self.symbol isEqualToString:@"+"]){
        result = self.firstNumber + self.secondNumber;
    }
    else if([self.symbol isEqualToString:@"-"]){
        result = self.firstNumber - self.secondNumber;
    }
    else if([self.symbol isEqualToString:@"x"]){
        result = self.firstNumber * self.secondNumber;
    }
    else if([self.symbol isEqualToString:@"÷"]){
        if(self.secondNumber == 0){
            result = NAN;
        }
        else{
            result = self.firstNumber / self.secondNumber;
        }
    }
    else if([self.symbol isEqualToString:@"%"]){
        result = (int)self.firstNumber % (int)self.secondNumber;
    }
    else{
        result = self.secondNumber;
    }
    
    if(isnan(result)){
        self.displayCalculation.text = @"Error";
    }
    else{
        self.displayCalculation.text = [NSString stringWithFormat:@"%g", result];
    }
}


@end
