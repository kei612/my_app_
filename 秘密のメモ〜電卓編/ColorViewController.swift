//
//  ColorViewController.swift
//  noteサンプル
//
//  Created by 伊藤慶 on 2016/09/25.
//  Copyright © 2016年 伊藤慶. All rights reserved.
//

import UIKit

class ColorViewController: UIViewController {

    @IBOutlet weak var color1: UIButton!
    @IBOutlet weak var color2: UIButton!
    @IBOutlet weak var color3: UIButton!
    @IBOutlet weak var color4: UIButton!
    @IBOutlet weak var color5: UIButton!
    @IBOutlet weak var color6: UIButton!
    @IBOutlet weak var color7: UIButton!
    @IBOutlet weak var color8: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    func  color(color:UIColor) {
        navigationController?.navigationBar.barTintColor = color
    }
    
    @IBAction func color1(_ sender: AnyObject) {
        color(color: color1.backgroundColor!)
    }
    @IBAction func color2(_ sender: AnyObject) {
        color(color: color2.backgroundColor!)
    }

    @IBAction func color3(_ sender: AnyObject) {
        color(color: color3.backgroundColor!)
    }

    @IBAction func color4(_ sender: AnyObject) {
        color(color: color4.backgroundColor!)
    }

    @IBAction func color5(_ sender: AnyObject) {
        color(color: color5.backgroundColor!)
    }

    @IBAction func color6(_ sender: AnyObject) {
        color(color: color6.backgroundColor!)
    }
    @IBAction func color7(_ sender: AnyObject) {
        color(color: color7.backgroundColor!)
    }
    @IBAction func color8(_ sender: AnyObject) {
        color(color: color8.backgroundColor!)
    }

    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
