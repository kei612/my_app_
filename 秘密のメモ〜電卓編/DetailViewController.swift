//
//  DetailViewController.swift
//  Lock notes
//
//  Created by 伊藤慶 on 2015/09/13.
//  Copyright (c) 2015年 伊藤慶. All rights reserved.
//

import UIKit
import Accounts

class DetailViewController: UIViewController, UITextViewDelegate,UINavigationControllerDelegate {

    @IBOutlet weak var toolbar: UIToolbar!
    @IBOutlet weak var textview: UITextView!
    @IBOutlet weak var bottomLayoutConstraint: NSLayoutConstraint!
    @IBOutlet weak var label:UILabel!
    var UserDafault:UserDefaults = UserDefaults()
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(keyboardWillChangeFrame(notification:)),
                                               name: NSNotification.Name.UIKeyboardWillChangeFrame,
                                               object: nil)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(keyboardWillHide(notification:)),
                                               name: NSNotification.Name.UIKeyboardWillHide,
                                               object: nil)
        self.textview.resignFirstResponder()

        if UserDafault.integer(forKey: "size") < 18 {
            textview.font = UIFont.systemFont(ofSize: CGFloat(18))
        }else{
            textview.font = UIFont.systemFont(ofSize: CGFloat(UserDafault.integer(forKey: "size")))
            textview.font = UIFont(name:UserDafault.object(forKey: "font") as! String,size:CGFloat(UserDafault.integer(forKey: "size")))
        }
        toolbar.barTintColor = navigationController?.navigationBar.barTintColor
    
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self)
        
        if textview.text == "" {
            allNotes.remove(at: Noteindex)
//            allNotes2.remove(at: Noteindex)
        }else{
            allNotes[Noteindex].note = textview.text
//            allNotes2[Noteindex].date = label.text!
            print(label.text)
            print(textview.text)
            print(label.text!)
            print(textview.text!)
            print(allNotes[Noteindex].note)
//            print(allNotes2[Noteindex].date)

            
        }
        Note.saveNotes()
//        Dates.saveNotes()
        NoteTable?.reloadData()
        
    }
    
    func keyboardWillChangeFrame(notification: NSNotification) {
        
        if let userInfo = notification.userInfo {
            
            let keyBoardValue : NSValue = userInfo[UIKeyboardFrameEndUserInfoKey]! as! NSValue
            let keyBoardFrame : CGRect = keyBoardValue.cgRectValue
            let duration : TimeInterval = userInfo[UIKeyboardAnimationDurationUserInfoKey]! as! TimeInterval
            
            self.bottomLayoutConstraint.constant = keyBoardFrame.size.height
            
            UIView.animate(withDuration: duration, animations: { () -> Void in
                self.view.layoutIfNeeded()
            })
            
        }
    }
    
    func keyboardWillHide(notification: NSNotification) {
        if let userInfo = notification.userInfo {
            
            let duration : TimeInterval = userInfo[UIKeyboardAnimationDurationUserInfoKey]! as! TimeInterval
            
            self.bottomLayoutConstraint.constant = 0
            
            UIView.animate(withDuration: duration, animations: { () -> Void in
                self.view.layoutIfNeeded()
            })
            
        }
    }
    @IBAction func end(_ sender: AnyObject) {
        self.textview.resignFirstResponder()
        
    }
    @IBAction func button(_ sender: AnyObject) {
        let activityItems = [textview.text] as [Any]
        // 初期化処理
        let activityVC = UIActivityViewController(activityItems: activityItems, applicationActivities: nil)
        
        
        // UIActivityViewControllerを表示
        self.present(activityVC, animated: true, completion: nil)
    }

    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
//        if allNotes2[Noteindex].date == "" {
            getDate()
//        }else{
//        label.text = allNotes2[Noteindex].date
//        }
        textview.text = allNotes[Noteindex].note
        textview.becomeFirstResponder()
        textview.delegate = self
        textview.dataDetectorTypes = UIDataDetectorTypes.link
        NotificationCenter.default.removeObserver(self)
        label.textColor = UIColor.lightGray
        self.automaticallyAdjustsScrollViewInsets = false
    }
    func getDate() {
        let date = Date()
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "ja_JP")
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm"
        label.text = dateFormatter.string(from: date) as String
        print(label.text! as String)
        
    }

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

