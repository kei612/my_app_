//
//  Step3ViewController.h
//  パスワードsample
//
//  Created by 伊藤慶 on 2016/05/21.
//  Copyright © 2016年 伊藤慶. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Step3ViewController : UIViewController
{
    UIImage *image[8];
    NSMutableArray *array;
    NSUserDefaults *defaults;
    BOOL *hantei;
    int count;
}
@property (weak, nonatomic) IBOutlet UIImageView *imageview;
@property (weak, nonatomic) IBOutlet UILabel *label;
@property (weak, nonatomic) IBOutlet UIButton *button;
@property BOOL volume;
@property BOOL hantei;
@property BOOL ok;
@end
