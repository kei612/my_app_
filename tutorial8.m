//
//  tutorial8.m
//  秘密のメモ〜電卓編
//
//  Created by 伊藤慶 on 2016/07/16.
//  Copyright © 2016年 伊藤慶. All rights reserved.
//

#import "tutorial8.h"

@interface tutorial8 ()

@end

@implementation tutorial8

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    //Pinch（2本の指でつまむ）の認識
    UIPinchGestureRecognizer *pinchRecognizer = [[UIPinchGestureRecognizer alloc] initWithTarget:self action:@selector(pinchDetected:)];
    [self.view addGestureRecognizer:pinchRecognizer];
    
    //Rotate（2本の指で回転）の認識
    UIRotationGestureRecognizer *rotationRecognizer = [[UIRotationGestureRecognizer alloc] initWithTarget:self action:@selector(rotationDetected:)];
    [self.view addGestureRecognizer:rotationRecognizer];
    
    //右向きのSwipe（1本指でなぞる）の認識
    UISwipeGestureRecognizer *swipeRightRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeRightDetected:)];
    swipeRightRecognizer.direction = UISwipeGestureRecognizerDirectionRight;
    [self.view addGestureRecognizer:swipeRightRecognizer];
    
    //左向きのSwipe（1本指でなぞる）の認識
    UISwipeGestureRecognizer* swipeLeftRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeLeftDetected:)];
    swipeLeftRecognizer.direction = UISwipeGestureRecognizerDirectionLeft;
    [self.view addGestureRecognizer:swipeLeftRecognizer];
    
    //上向きのSwipe（1本指でなぞる）の認識
    UISwipeGestureRecognizer *swipeUpRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeUpDetected:)];
    swipeUpRecognizer.direction = UISwipeGestureRecognizerDirectionUp;
    [self.view addGestureRecognizer:swipeUpRecognizer];
    
    //下向きのSwipe（1本指でなぞる）の認識
    UISwipeGestureRecognizer* swipeDownRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeDownDetected:)];
    swipeDownRecognizer.direction = UISwipeGestureRecognizerDirectionDown;
    [self.view addGestureRecognizer:swipeDownRecognizer];
    [self layer:_imageview1];
    [self layer:_imageview2];
    [self layer:_imageview3];
    [self layer:_imageview4];
    views[0]=_imageview1;
    views[1]=_imageview2;
    views[2]=_imageview3;
    views[3]=_imageview4;
   
    
    image[0] = [UIImage imageNamed:@"Swipe_Right.png"];
    image[1] = [UIImage imageNamed:@"Swipe_Left.png"];
    image[2] = [UIImage imageNamed:@"Swipe_Up.png"];
    image[3] = [UIImage imageNamed:@"Swipe_Down.png"];
    image[4] = [UIImage imageNamed:@"2x_Rotate_Right.png"];
    image[5] = [UIImage imageNamed:@"2x_Rotate_Left.png"];
    image[6] = [UIImage imageNamed:@"Spread.png"];
    image[7] = [UIImage imageNamed:@"Pinch.png"];
   
}
- (void)viewWillAppear:(BOOL)animated
{
    NSLog(@"viewWillAppear");
    hantei=YES;
    hantei2=YES;
    next=NO;
    array= [NSMutableArray array];
    array2= [NSMutableArray array];
    array3= [NSMutableArray array];
    array4= [NSMutableArray array];
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    NSData *data = [ud objectForKey:@"password"];
    array2 = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    


}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)layer:(UIImageView *)imageview{
    [[imageview layer] setBorderColor:[[UIColor colorWithRed:256 green:256 blue:256 alpha:1] CGColor]];
    [[imageview layer] setBorderWidth:3.0];
}
-(void)check:(int)number{

        if (array.count<4) {
            [array addObject:[NSNumber numberWithInteger:number]];
            for (int i=0;i<array.count; i++) {
                views[i].image=image[[[array objectAtIndex:i] intValue]];
            }
            _imageview.image=image[number];
            if(array.count==4){
                for (int i=0;i<array.count; i++) {
                    if ([[array objectAtIndex:i] intValue]==[[array2 objectAtIndex:i] intValue]) {
                        
                    }else{
                        
                        hantei=NO;
                       
                    }
                }

                if (hantei==YES) {
                    next=YES;
                    _label.text=@"新しいジェスチャーパスワードを入力";
                    _imageview.image=nil;
                    _imageview1.image=nil;
                    _imageview2.image=nil;
                    _imageview3.image=nil;
                    _imageview4.image=nil;
                }else{
                    //                    //label変更
                    _label.text=@"パスワードが一致しません\nもう一度やり直してください";
                    _imageview.image=nil;
                    _imageview1.image=nil;
                    _imageview2.image=nil;
                    _imageview3.image=nil;
                    _imageview4.image=nil;
                    [array removeAllObjects];
                }

            }
            
        
    
    NSLog(@"YESNO: %@",(hantei == 1 ? @"YES":@"NO"));
 
    }
}
-(void)newpassword:(int)count{
        if (hantei2==YES) {
            hantei=nil;
        if (array3.count<4) {
            [array3 addObject:[NSNumber numberWithInteger:count]];
            NSLog(@"%lu",(unsigned long)array3.count);
            for (int i=0;i<array3.count; i++) {
                views[i].image=image[[[array3 objectAtIndex:i] intValue]];
            }
            _imageview.image=image[count];
            if(array3.count==4){
                
                _label.text=@"パスワードを確認してください";
                //画像のリセット
                _imageview.image=nil;
                _imageview1.image=nil;
                _imageview2.image=nil;
                _imageview3.image=nil;
                _imageview4.image=nil;
                //パスワードが確認
                hantei2=NO;
            }
            
        }
    }else{
        if (array4.count<4) {
            [array4 addObject:[NSNumber numberWithInteger:count]];
            for (int i=0;i<array4.count; i++) {
                views[i].image=image[[[array4 objectAtIndex:i] intValue]];
            }
            _imageview.image=image[count];
            if(array4.count==4){
                for (int i=0;i<array4.count; i++) {
                    if ([[array3 objectAtIndex:i] intValue]==[[array4 objectAtIndex:i] intValue]) {
                        
                    }else{
                        
                        
                        hantei2=YES;
                        //                        break;
                    }
                }
                if (hantei2==NO) {
                    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                    [defaults removeObjectForKey:@"password"];
                    NSData *listData = [NSKeyedArchiver archivedDataWithRootObject:array4];
                    [defaults setObject:listData forKey:@"password"];
                    [defaults synchronize];
                    [self performSegueWithIdentifier:@"newnext" sender:self];
                }else{
                    //                    //label変更
                    _label.text=@"パスワードが一致しません\nもう一度やり直してください";
                    _imageview.image=nil;
                    _imageview1.image=nil;
                    _imageview2.image=nil;
                    _imageview3.image=nil;
                    _imageview4.image=nil;
                    [array3 removeAllObjects];
                    [array4 removeAllObjects];
                }
                
            }
        }}
    NSLog(@"YESNO: %@",(hantei == 1 ? @"YES":@"NO"));
}

-(IBAction)back:(id)sender{

  
    _imageview.image=nil;
    _imageview1.image=nil;
    _imageview2.image=nil;
    _imageview3.image=nil;
    _imageview4.image=nil;
    if (hantei==YES) {
        [array removeLastObject];

        for (int i=0;i<array.count; i++) {
            views[i].image=image[[[array objectAtIndex:i] intValue]];
        }
    }
    if(hantei2==YES){
         [array3 removeLastObject];
            for (int i=0;i<array3.count; i++) {
                views[i].image=image[[[array3 objectAtIndex:i] intValue]];
            }
            
    }
    if(hantei2==NO){
          [array4 removeLastObject];
        for (int i=0;i<array4.count; i++) {
            views[i].image=image[[[array4 objectAtIndex:i] intValue]];
        }
}
}
- (IBAction)backs:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];

}
- (IBAction)swipeRightDetected:(UIGestureRecognizer *)sender {
    NSLog(@"右向きSwipe");
    if (next==NO) {
    [self check:0];
    }else{
    [self newpassword:0];
    }
}

//左向きスワイプ検知時に呼ばれるメソッド
- (IBAction)swipeLeftDetected:(UIGestureRecognizer *)sender {
    NSLog(@"左向きSwipe");
    if (next==NO) {
        [self check:1];
    }else{
        [self newpassword:1];
    }
}

//上向きスワイプ検知時に呼ばれるメソッド
- (IBAction)swipeUpDetected:(UIGestureRecognizer *)sender {
    NSLog(@"上向きSwipe");
    if (next==NO) {
        [self check:2];
    }else{
        [self newpassword:2];
    }
    
}

//下向きスワイプ検知時に呼ばれるメソッド
- (IBAction)swipeDownDetected:(UIGestureRecognizer *)sender {
    NSLog(@"下向きSwipe");
    if (next==NO) {
        [self check:3];
    }else{
        [self newpassword:3];
    }
    
}

- (IBAction)rotationDetected:(UIGestureRecognizer *)sender {
    //Rotate開始時から見た回転の度合い（ラジアン）
    CGFloat radians = [(UIRotationGestureRecognizer *)sender rotation];
    //「ラジアン」を「度」に変換
    CGFloat degrees = radians * (180/M_PI);
    if (sender.state == UIGestureRecognizerStateEnded) {
        
        
        if (degrees > 90) {
            if (next==NO) {
                [self check:4];
            }else{
                [self newpassword:4];
            }
            NSLog(@"時計回りにRotate degrees: %f", degrees);
        } else if (degrees < -90) {
            if (next==NO) {
                [self check:5];
            }else{
                [self newpassword:5];
            }
            NSLog(@"反時計回りにRotate degrees: %f", degrees);
        }
    }
}
//ピンチ動作検知時に呼ばれるメソッド
- (IBAction)pinchDetected:(UIGestureRecognizer *)sender {
    //ピンチ開始の2本の指の距離を1とした時
    //現在の2本の指の相対距離
    CGFloat scale = [(UIPinchGestureRecognizer *)sender scale];
    
    if (scale > 2.4) {
        NSLog(@"外向きにPinch scale: %f", scale);
        if (sender.state == UIGestureRecognizerStateEnded) {
            
            if (next==NO) {
                [self check:6];
            }else{
                [self newpassword:6];
            }
        }
    } else if (scale < 0.4) {
        NSLog(@"内向きにPinch scale: %f", scale);
        if (sender.state == UIGestureRecognizerStateEnded) {
            
            if (next==NO) {
                [self check:7];
            }else{
                [self newpassword:7];
            }
        }
        
    }
}@end
