//
//  tutorial7.m
//  秘密のメモ〜電卓編
//
//  Created by 伊藤慶 on 2016/07/16.
//  Copyright © 2016年 伊藤慶. All rights reserved.
//

#import "tutorial7.h"

@interface tutorial7 ()

@end

@implementation tutorial7

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    views[0]=_imageview1;
    views[1]=_imageview2;
    views[2]=_imageview3;
    views[3]=_imageview4;
    [self layer:_imageview1];
    [self layer:_imageview2];
    [self layer:_imageview3];
    [self layer:_imageview4];

    image[0] = [UIImage imageNamed:@"Swipe_Right.png"];
    image[1] = [UIImage imageNamed:@"Swipe_Left.png"];
    image[2] = [UIImage imageNamed:@"Swipe_Up.png"];
    image[3] = [UIImage imageNamed:@"Swipe_Down.png"];
    image[4] = [UIImage imageNamed:@"2x_Rotate_Right.png"];
    image[5] = [UIImage imageNamed:@"2x_Rotate_Left.png"];
    image[6] = [UIImage imageNamed:@"Spread.png"];
    image[7] = [UIImage imageNamed:@"Pinch.png"];
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    NSData *data = [ud objectForKey:@"password"];
    array = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    for (int i=0;i<array.count; i++) {
        views[i].image=image[[[array objectAtIndex:i] intValue]];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)layer:(UIImageView *)imageview{
    [[imageview layer] setBorderColor:[[UIColor colorWithRed:256 green:256 blue:256 alpha:1] CGColor]];
    [[imageview layer] setBorderWidth:3.0];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
