//
//  tutorial9.m
//  秘密のメモ〜電卓編
//
//  Created by 伊藤慶 on 2016/08/08.
//  Copyright © 2016年 伊藤慶. All rights reserved.
//

#import "tutorial9.h"

@interface tutorial9 ()

@end

@implementation tutorial9

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    _isFadeIn = YES;
    self.transitioningDelegate=self;
    NSTimer *tm =
    [NSTimer
     scheduledTimerWithTimeInterval:1.5
     target:self
     selector:@selector(FadeInOut:)
     userInfo:nil
     repeats:YES
     ];
    //上向きのSwipe（1本指でなぞる）の認識
    UISwipeGestureRecognizer *swipeUpRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeUpDetected:)];
    swipeUpRecognizer.direction = UISwipeGestureRecognizerDirectionUp;
    [self.view addGestureRecognizer:swipeUpRecognizer];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)swipeUpDetected:(UIGestureRecognizer *)sender {
    NSLog(@"上向きSwipe");
    [self performSegueWithIdentifier:@"stepx" sender:self];
}
-(void)FadeInOut:(NSTimer*)timer{
    if (_isFadeIn) {
        //フェードアウトアニメーション実行
        [self sampleImageFadeOut];
    } else {
        //フェードインアニメーション実行
        [self sampleImageFadeIn];
    }
    _isFadeIn = !_isFadeIn;
    
}

- (void)sampleImageFadeIn
{
    //フェードイン
    _ImageView.alpha = 0;
    //アニメーションのタイプを指定
    [UIView beginAnimations:@"fadeIn" context:nil];
    //イージング指定
    [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
    //アニメーション秒数を指定
    [UIView setAnimationDuration:0.9];
    //目標のアルファ値を指定
    _ImageView.alpha = 1;
    //アニメーション実行
    [UIView commitAnimations];
}

- (void)sampleImageFadeOut
{
    //フェードアウト
    [UIView beginAnimations:@"fadeOut" context:nil];
    //イージング指定
    [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
    //アニメーション秒数を指定
    [UIView setAnimationDuration:0.8];
    //目標のアルファ値を指定
    _ImageView.alpha = 0;
    //アニメーション実行
    [UIView commitAnimations];
}
- (id <UIViewControllerAnimatedTransitioning>)animationControllerForPresentedController:(UIViewController *)presented
                                                                   presentingController:(UIViewController *)presenting
                                                                       sourceController:(UIViewController *)source
{
    animation2 *animation=[[animation2 alloc]init];
    return animation;
    
}

- (id <UIViewControllerAnimatedTransitioning>)animationControllerForDismissedController:(UIViewController *)dismissed
{
    animation2 *animation=[[animation2 alloc]init];
    return animation;
}

@end
