//
//  tutorial4.m
//  秘密のメモ〜電卓編
//
//  Created by 伊藤慶 on 2016/07/15.
//  Copyright © 2016年 伊藤慶. All rights reserved.
//

#import "tutorial4.h"

@interface tutorial4 ()

@end

@implementation tutorial4

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.transitioningDelegate=self;
    _isFadeIn = YES;
    NSTimer *tm =
    [NSTimer
     scheduledTimerWithTimeInterval:1.5
     target:self
     selector:@selector(FadeInOut:)
     userInfo:nil
     repeats:YES
     ];
    //Rotate（2本の指で回転）の認識
    UIRotationGestureRecognizer *rotationRecognizer = [[UIRotationGestureRecognizer alloc] initWithTarget:self action:@selector(rotationDetected:)];
    [self.view addGestureRecognizer:rotationRecognizer];
    

    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)rotationDetected:(UIGestureRecognizer *)sender {
    //Rotate開始時から見た回転の度合い（ラジアン）
    CGFloat radians = [(UIRotationGestureRecognizer *)sender rotation];
    //「ラジアン」を「度」に変換
    CGFloat degrees = radians * (180/M_PI);
    if (sender.state == UIGestureRecognizerStateEnded) {
        
        
        if (degrees > 90) {
             [self performSegueWithIdentifier:@"step4" sender:self];
        } else if (degrees < -90) {
        }
    }
}
-(void)FadeInOut:(NSTimer*)timer{
    if (_isFadeIn) {
        //フェードアウトアニメーション実行
        [self sampleImageFadeOut];
    } else {
        //フェードインアニメーション実行
        [self sampleImageFadeIn];
    }
    _isFadeIn = !_isFadeIn;
    
}

- (void)sampleImageFadeIn
{
    //フェードイン
    _ImageView.alpha = 0;
    //アニメーションのタイプを指定
    [UIView beginAnimations:@"fadeIn" context:nil];
    //イージング指定
    [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
    //アニメーション秒数を指定
    [UIView setAnimationDuration:0.9];
    //目標のアルファ値を指定
    _ImageView.alpha = 1;
    //アニメーション実行
    [UIView commitAnimations];
}

- (void)sampleImageFadeOut
{
    //フェードアウト
    [UIView beginAnimations:@"fadeOut" context:nil];
    //イージング指定
    [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
    //アニメーション秒数を指定
    [UIView setAnimationDuration:0.8];
    //目標のアルファ値を指定
    _ImageView.alpha = 0;
    //アニメーション実行
    [UIView commitAnimations];
}
- (id <UIViewControllerAnimatedTransitioning>)animationControllerForPresentedController:(UIViewController *)presented
                                                                   presentingController:(UIViewController *)presenting
                                                                       sourceController:(UIViewController *)source
{
    animation4 *animation=[[animation4 alloc]init];
    return animation;
    
}

- (id <UIViewControllerAnimatedTransitioning>)animationControllerForDismissedController:(UIViewController *)dismissed
{
    animation4 *animation=[[animation4 alloc]init];
    return animation;
}


@end
